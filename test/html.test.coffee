SSBot = require '..'
assert = require 'assert'

describe 'Clean HTML code to Telegram', ->

    it 'Clean <br>', ->
        assert.equal 'aaa\nbbb\nccc', SSBot.cleanHTML 'aaa<br>bbb<br>ccc'

    it 'Clean <br> ignore line break', ->
        assert.equal 'aaa\nbbb\nccc', SSBot.cleanHTML 'aaa\n<br>bbb<br>\nccc'

    it 'Clean <br><br> is like a <p>', ->
        assert.equal 'aaa\n\nbbb', SSBot.cleanHTML 'aaa<br><br>bbb'

    it 'Not clean <b> <i> <a> <strong> <em> <code> <pre>', ->
        assert.equal 'aaa\n<b>bbb</b>ccc', SSBot.cleanHTML 'aaa<br><b>bbb</b>ccc'
        assert.equal 'aaa\n<i>bbb</i>ccc', SSBot.cleanHTML 'aaa<br><i>bbb</i>ccc'
        assert.equal 'aaa\n<a href="url">bbb</a>ccc', SSBot.cleanHTML 'aaa<br><a href="url">bbb</a>ccc'
        assert.equal 'aaa\n<strong>bbb</strong>ccc', SSBot.cleanHTML 'aaa<br><strong>bbb</strong>ccc'
        assert.equal 'aaa\n<em>bbb</em>ccc', SSBot.cleanHTML 'aaa<br><em>bbb</em>ccc'
        assert.equal 'aaa\n<code>bbb</code>ccc', SSBot.cleanHTML 'aaa<br><code>bbb</code>ccc'
        assert.equal 'aaa\n<pre>bbb</pre>ccc', SSBot.cleanHTML 'aaa<br><pre>bbb</pre>ccc'

    it 'Closes unclosed tags', ->
        assert.equal 'aaa<i>bbb</i>', SSBot.cleanHTML 'aaa<i>bbb'

    it 'Remove closed, but unopened tags', ->
        assert.equal 'aaabbb', SSBot.cleanHTML 'aaa</b>bbb'
        assert.equal 'aaabbb', SSBot.cleanHTML 'aaa</strong>bbb'

    it 'Clean <img> tags', ->
        assert.equal 'some text ok!',
            SSBot.cleanHTML 'some text <img src="foo"> ok!'

        assert.equal 'some text FOO ok!',
            SSBot.cleanHTML 'some text <img src="foo" alt="FOO"> ok!'

    it 'Clean a page like html', ->
        assert.equal '''
        <strong>Test Page</strong>
        
        This is <b>only</b> a <a href="test">test</a>.
        
        “2&gt;1”!
        ''',
            SSBot.cleanHTML '''<!doctype html>
            <html>
            <head>
              <title> Test Page - My Site </title>
              <style type="text/css">
                body { color: green }
                .main { font-size: 200% }
              </style>
            </head>
            <body>
              <h1> Test Page</strong> </h1>
              <!-- this is a comment -->
              <script> alert('noise') </script>
              <p> This is <b>only</b> a <a href="test"><b>test</b></a>. </p>
              <!-- this is another comment -->
              <!--[if garbage]> nothing <![endif]-->
              <blockquote> &ldquo;2&gt;1&rdquo;&#33; </blockquote>
            </body>
            </html>'''

describe 'Remove nested tags', ->

    it 'remove direct nest', ->
        assert.equal 'xxx<a>yyy</a>zzz',
            SSBot.removeNestedTags 'xxx<a><b>yyy</b></a>zzz'
        assert.equal 'xxx<a href="url">yyy</a>zzz',
            SSBot.removeNestedTags 'xxx<a href="url"><b>yyy</b></a>zzz'

    it 'remove indirect nest', ->
        assert.equal 'xxx<a>XyyyZ</a>zzz',
            SSBot.removeNestedTags 'xxx<a>X<b>yyy</b>Z</a>zzz'

    it 'remove direct 3 level nest', ->
        assert.equal 'xxx<a>yyy</a>zzz',
            SSBot.removeNestedTags 'xxx<a><b><b>yyy</b></b></a>zzz'

    it 'remove neigbor nests', ->
        assert.equal 'xxx<a>AAA</a>yyy<a>BBB</a>zzz',
            SSBot.removeNestedTags 'xxx<a><b>AAA</b></a>yyy<a><b>BBB</b></a>zzz'

    it 'remove nested neigbors', ->
        assert.equal 'xxx<a>AAABBB</a>zzz',
            SSBot.removeNestedTags 'xxx<a><b>AAA</b><b>BBB</b></a>zzz'

    it 'remove nested [2,1] neigbors', ->
        assert.equal 'xxx<a>AAABBB</a>zzz',
            SSBot.removeNestedTags 'xxx<a><b><i>AAA</i></b><b>BBB</b></a>zzz'

    it 'remove nested [1,2] neigbors', ->
        assert.equal 'xxx<a>AAABBB</a>zzz',
            SSBot.removeNestedTags 'xxx<a><b>AAA</b><b><i>BBB</i></b></a>zzz'

    it 'remove nested [2,2] neigbors', ->
        assert.equal 'xxx<a>AAABBB</a>zzz',
            SSBot.removeNestedTags 'xxx<a><b><i>AAA</i></b><b><i>BBB</i></b></a>zzz'

describe 'Remove tag attributes', ->

    it 'clear tags with one attr', ->
        assert.equal '<b>test</b>',
            SSBot.cleanHTML '<b class="foo">test</b>'
        assert.equal '<b>test1</b> <b>test2</b>',
            SSBot.cleanHTML '<b class="foo">test1</b> <b class="bar">test2</b>'

    it 'clear tags with many attr', ->
        assert.equal '<b>test</b>',
            SSBot.cleanHTML '<b class="foo" title="wow">test</b>'
        assert.equal '<b>test</b>',
            SSBot.cleanHTML '<b class="foo" title="wow" data-x="123">test</b>'
        assert.equal '<b>test1</b> <b>test2</b>',
            SSBot.cleanHTML '<b class="foo" title="wow">test1</b> <b class="bar" title="ok">test2</b>'

    it 'clear tag link, but href', ->
        assert.equal '<a href="foo">test</a>',
            SSBot.cleanHTML '<a href="foo">test</a>'
        assert.equal '<a href="foo">test</a>',
            SSBot.cleanHTML '<a href="foo" class="wow">test</a>'
        assert.equal '<a href="foo">test1</a> <a href="bar">test2</a>',
            SSBot.cleanHTML '<a href="foo" class="wow">test1</a> <a href="bar" class="ok">test2</a>'

describe 'Display lists', ->

    it 'Simple list', ->
        assert.equal '''My list:
            • Babana
            • Apple
            • Strawberry''',
            SSBot.cleanHTML '''My list: <ul>
            <li>Babana</li>
            <li>Apple</li>
            <li>Strawberry</li>
            </ul>'''

    it 'Neigbor simple list', ->
        assert.equal '''Lists:
            • Babana
            • Apple

            • Cat
            • Dog''',
            SSBot.cleanHTML '''Lists:
            <ul><li>Babana</li><li>Apple</li></ul>
            <ul><li>Cat</li><li>Dog</li></ul>'''

describe 'Display <hr>', ->

    it 'One <hr>', ->
        assert.equal 'something\n\n─────────────────────────\n\notherthing',
            SSBot.cleanHTML 'something<hr>otherthing'

    it 'Neigbor <hr>s', ->
        assert.equal '''something
        
        ─────────────────────────
        
        ─────────────────────────
        
        otherthing''',
            SSBot.cleanHTML 'something<hr><hr>otherthing'

describe 'limit Text size', ->
    str3285 = ('x' for i in [1..3285]).join ''
                        #123456789012345
    str3300 = str3285 + 'abcdefghijklmno'
    assert.equal 3300, str3300.length

    it 'not cut a string until 3300 by deafult', ->
        assert.equal str3285, SSBot.limitText str3285
        assert.equal str3300, SSBot.limitText str3300

    it 'cut a string bigger than 3300 by deafult', ->
        str = str3300 + 'X'
        assert.equal str3285+'abcde…', SSBot.limitText str

    it 'cut a string bigger than defined length', ->
        assert.equal 'abcdefghijklmno', SSBot.limitText('abcdefghijklmno', length:15)
        assert.equal 'abcde…', SSBot.limitText('abcdefghijklmnop', length:15)

    it 'cut html code', ->
        assert.equal 'abc<b>def</b>ghij', SSBot.limitText('abc<b>def</b>ghij', isHTML:true, length:17)
        assert.equal 'abc<b>d.</b>…', SSBot.limitText('abc<b>d.f</b>ghijk', isHTML:true, length:17)
        assert.equal 'abc<b></b>…', SSBot.limitText('abc<b>def</b>ghijk', isHTML:true, length:17)

    it 'cut html code with tag bigger than one letter', ->
        assert.equal 'a<pre>bc</pre>def', SSBot.limitText('a<pre>bc</pre>def', isHTML:true, length:17)
        assert.equal 'a<pre>b.</pre>…', SSBot.limitText('a<pre>b.c</pre>def', isHTML:true, length:17)
        assert.equal 'a<pre></pre>…', SSBot.limitText('a<pre>bc</pre>defg', isHTML:true, length:17)

    it 'cut links', ->
        assert.equal 'a <a href="foo"> b c </a> de',
            SSBot.limitText('a <a href="foo"> b c </a> de', isHTML:true, length:28)
        assert.equal 'a <a href="foo"> b</a>…',
            SSBot.limitText('a <a href="foo"> b c </a> def', isHTML:true, length:28)



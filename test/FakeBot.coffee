exports.FakeBot = class FakeBot
    constructor: ()->
        @username = 'FakeBot'
        @hist = []

    logErrorAndMsgAdm: (err, msg)->
        console.error msg
        throw err

    sendMessage: (text, chatId, conf={})->
        if conf.reply_markup
            conf = {
                conf...
                reply_markup: JSON.parse conf.reply_markup
            }
        @hist.unshift {
            conf...
            type: 'message'
            chatId, text
        }

    editMessageTextOrReply: (text, chatId, msgId, conf={})->
        if conf.reply_markup
            conf = {
                conf...
                reply_markup: JSON.parse conf.reply_markup
            }
        @hist.unshift {
            conf...
            type: 'edit_message'
            chatId, msgId, text
        }

    answerCallbackQuery: (queryId, text, showAlert)->
        @hist.unshift {
            type: 'answer_cb_query'
            queryId, text, showAlert
        }

exports.mkUpdate = (kind, text, conf={})->
    conf._act ?= {}
    conf._act.message ?= {}
    conf._act.from ?= {}
    {
        _kind: kind
        _act: {
            conf._act...
            text: text
            chat: id: conf.chatId
            message: {
                conf._act.message...
                text: text
                message_id: conf.msgId
            }
            from: {
                conf._act.from...
                username: conf.username || 'theuser'
            }
        }
    }

exports.mkMsgUpdate = (text, conf={})->
    exports.mkUpdate 'message', text, conf

exports.mkCbQueryUpdate = (text, data, conf={})->
    conf._act ?= {}
    conf._act.data = data
    exports.mkUpdate 'callback_query', text, conf


{ timeout } = require '../helpers/timing'
{ randText, lastGlobalRandChoicesMemo, setClearMemoDelay } = require '../helpers/random'
assert = require 'assert'

setClearMemoDelay 1

describe 'Random Helper', ->

    it 'randText select one option from two without replacer', ->
        choices = ['txt1', 'txt2']
        assert.match randText('key1', choices), /^txt[12]$/

    it 'randText select one option from one without replacer', ->
        choices = ['txt1']
        assert.match randText('key2', choices), /^txt1$/
        assert.match randText('key2', choices), /^txt1$/

    it 'randText select do not repeat until options end', ->
        choices = ['txt1', 'txt2', 'txt3']
        results = []
        results.push randText('key3', choices)
        results.push randText('key3', choices)
        assert.notEqual results[0], results[1]
        assert.equal results.indexOf(randText 'key3', choices), -1
        assert.match randText('key3', choices), /^txt[123]$/

    it 'randText forgot choices after timeout', (done)->
        setClearMemoDelay .3
        choices = ['txt1', 'txt2', 'txt3', 'txt4']
        randText 'key4', choices
        randText 'key4', choices
        assert.equal lastGlobalRandChoicesMemo.key4.length, 2
        timeout .5, ->
            assert.equal lastGlobalRandChoicesMemo.key4, null
            randText 'key4', choices
            assert.equal lastGlobalRandChoicesMemo.key4.length, 1
            do done

    it 'randText select one option and replace keys', ->
        choices = ['txt1 {{x}}', 'txt2 {{x}}']
        assert.match randText('key5', choices, x:123), /^txt[12] 123$/


    it 'randText replace only given keys', ->
        choices = ['txt {{x}} {{y}} {{z}}.']
        assert.equal randText('key6', choices, x:11, z:33), 'txt 11 {{y}} 33.'



fs = require 'fs'
stream = require 'stream'
request = require 'request'
htmlEntities = new (require('html-entities').AllHtmlEntities)
minify = require('html-minifier').minify
{ timeout, interval } = require './helpers/timing'

atFunctionRE = /at ([^\s]+) \((.+):([0-9]+):([0-9]+)\)/
atMainBodyRE = /at ()(.+):([0-9]+):([0-9]+)/

defaultLogger =
    log: (args...)->
        info = {}
        Error.captureStackTrace info
        at = info.stack.replace(/^\s*|\s*$/gm,'').split('\n')[2].replace /\s\(.*\)/, ''
        # TODO converter o log para JSON
        #atMatch = at.match atFunctionRE
        #atMatch ?= at.match atMainBodyRE
        #atMatch ?= [null, '', at]
        #atFunc = if atMatch[1] is '' then null else atMatch[1]
        #at = function: atFunc, file: atMatch[2], line: parseInt(atMatch[3]), column: parseInt(atMatch[4])
        console.log (new Date).toISOString().replace(/\..*/, ''), at, '⇒', args...
        console.log '' # quebra linha
    error: (args...)->
        info = {}
        Error.captureStackTrace info
        at = info.stack.replace(/^\s*|\s*$/gm,'').split('\n')[2].replace /\s\(.*\)/, ''
        console.error (new Date).toISOString().replace(/\..*/, ''), at, '⇒', args...
        console.log '' # quebra linha

mkID = -> (new Date).toISOString().replace(/.*T(.*)\..*/, '$1-') + Math.random().toString(16).split('.')[1]

getRealFileToSend = (file)->
    throw Error 'No file was provided.' unless file
    if 'string' is typeof file
        # If it looks like an URL, send it to telegram. They'll download.
        return file if /^https?:/.test file
        # Otherwise, it must be a file path to upload.
        return fs.createReadStream file
    # If the user gives a ReadStream upload it directily:
    return file if file instanceof fs.ReadStream
    return file if file instanceof stream.Readable
    #TODO: upload a raw file data (like from readFile) that is not a ReadStream.
    # If the user gives a object with an id it must be the file's id on Telegram.
    return file.id if file.id
    throw Error 'Unknown input type.'

class SmallSimpleBot

    constructor: (config={})->
        if 'string' is typeof config
            @token = config
        else
            @token = config.token
        unless config.autoTrap is false
            for sig in 'SIGTERM SIGINT SIGHUP SIGBREAK'.split(' ')
                do (sig)=> process.on sig, => @trapSignal sig
        @adms = []
        @addAdm adm for adm in config.adms if config.adms
        @sharedDataDir = config.dataDir
        @__webhookPath = config.webhookPath
        @__interactions = []
        @helpers =
            calendar: require('./helpers/calendar') this
            timing: require './helpers/timing'
            random: require './helpers/random'
        @logger = defaultLogger

    apiURL: (endpoint)-> "https://api.telegram.org/bot#{@token}/#{endpoint}"

    addAdm: (adm)->
        throw Error 'Adm object must have `username` attribute' unless adm.username?
        throw Error 'Adm object must have `id` attribute' unless adm.id?
        @adms.push adm

    isAdm: (username)-> username in @adms.map((adm)->adm.username)

    msgToAdm: (text, callback=(->))-> @sendMessage text, @adms[0].id, {}, callback
    msgToAdms: (text)-> @sendMessage text, adm.id for adm in @adms

    logErrorAndMsgAdm: (error, info, callback)->
        errMsg = error.message.substr 0, 512
        if info
            @logger.error info, error
            @msgToAdm info + '\n' + errMsg, callback
        else
            @logger.error error
            @msgToAdm errMsg, callback

    updateIdentity: (callback)->
        request @apiURL('getMe'), (error, response, body)=>
            return callback error if error
            identity = JSON.parse(body).result
            throw Error "Initialization Fail! #{body}" unless identity
            @name = identity.first_name
            @username = identity.username
            do callback

    run: ->
        @running = true
        @updateIdentity (error)=>
            return @logger.log error if error
            @lastUpdateFile = process.env.HOME + "/.local/#{@username}-last-update"
            @logger.log "Started! Name #{@name}, user @#{@username}"
            try
                @lastUpdate = fs.readFileSync @lastUpdateFile
            catch
                @logger.log "Last Update is 0. First Time?"
                @lastUpdate = 0
            @recoverRequestInterval = interval 2, => if do @lastRequestUpdateTimout
                @logger.log 'Recovering broken requestUpdate loop.' if @__lastRequestUpdate?
                do @__requestUpdate

    bindServer: (server)->
        @running = true
        @updateIdentity (error)=> @logger.log error if error
        @lastUpdateFile = '/dev/null'
        server.on 'request', (req, res)=>
            @logger.log 'Touch webhook, but not running.' unless @running
            if req.url is @__webhookPath and @running
                body = ''
                req.on 'data', (chunk)-> body += chunk.toString()
                req.on 'end', =>
                    try
                        @__incomingUpdate body
                        do res.end
                    catch err
                        @logger.error 'ERROR!!!', body, err
                        body = body.substr(0, 300) + '...' if body.length > 300
                        @msgToAdm """
                            #{err.message}

                            Incoming update body:
                            #{body}"

                            #{err.stack}
                            """

    trapSignal: (signal)->
        @logger.log "Existing by signal #{signal}..."
        @stop true

    stop: (@forceStop=false)->
        @running = false
        clearInterval @recoverRequestInterval
        clearTimeout @requestUpdateTimeout
        if @forceStop
            timeout 10, -> process.exit 0

    lastRequestUpdateTimout: -> (@__lastRequestUpdate or 0) < Date.now() - 50*1000

    normalizeUpdate: (update)->
        #@logger.log 'UPDATE =>', update
        update._kind = Object.keys(update).filter((k)-> k isnt 'update_id')[0]
        update._act = update[update._kind]
        update._act.from ?= {username: '(?)', error: Error 'From undefined'}
        update._act.chat ?= update._act.message?.chat or {title: '(?)', error: Error 'Chat undefined'}
        update._act.date = new Date (update._act.date * 1000 or Date.now())
        update._act.minutesAgo = (Date.now()-update._act.date.valueOf())/(1000*60)
        chat = update._act.chat
        @logger.log "Received #{update._kind} from
            #{update._act.from.username or update._act.from.first_name} in
            #{
                if chat.type is 'private'
                    '[private chat]'
                else
                    chat.title or chat.username or JSON.stringify(chat)
            }:
            #{update.message?.text or update.message?.caption or '(no text)'}"

    __requestUpdate: ->
        @logger.log 'Request Update...'
        @__lastRequestUpdate = Date.now()
        form = limit: 10, timeout: 40, offset: @lastUpdate+1
        request @apiURL('getUpdates'), form: form, (error, response, body)=>
            try
                return @logger.error error if (error)
                return @logger.error 'Invalid response', body if response.statusCode > 299
                @__incomingUpdate body
                if @running
                    # fast loop only if there is no error.
                    @requestUpdateTimeout = timeout .5, => do @__requestUpdate
                else if @forceStop
                    @logger.log 'Not running, Exiting.'
                    process.exit 0
                else
                    @logger.log 'Not running.'
            catch err
                @logger.error 'ERROR!!!', body, err

    __incomingUpdate: (body)->
        data = JSON.parse body or '{}'
        throw Error 'Update data is not ok.' unless data.ok or data.update_id
        data = { result: [ data ] } unless data.result?.forEach
        for update in data.result
            @normalizeUpdate update
            @lastUpdate = update.update_id
            fs.writeFileSync @lastUpdateFile, @lastUpdate.toString()
            privateChat = update.message?.chat?.type is 'private'
            callMe = update.message?.text?.indexOf(@username) > -1
            replyMe = false # TODO
            update.toMe = privateChat or callMe or replyMe or update._kind is 'callback_query'
            if update._act.minutesAgo > 5
                @logger.error "Ignored #{Math.round update._act.minutesAgo} minutes old update."
                #for interaction, i in @__interactions when interaction.ignoreTimeout
                #    ((act)=> timeout i/10, => act this, update) interaction
                @__interactions.map (interaction, i)=> do (update)=>
                    if interaction.ignoreTimeout
                        timeout i/10, => interaction this, update
            else
                @__interactions.map (interaction, i)=> do (update)=>
                    timeout i/10, => interaction this, update

    getChat: (chat_id, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} getChat of #{chat_id}."
        request.get @apiURL('getChat'), formData: {chat_id}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} getChat of #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} getChat of #{chat_id}"
            callback error, response, data?.result

    sendChatAction: (action, chat_id, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} sendChatAction in #{chat_id}:", action
        request.post @apiURL('sendChatAction'), formData: {chat_id, action}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} sendChatAction in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} sendChatAction in #{chat_id}"
            callback error, response, data?.result

    sendMessage: (text='', chat_id, params={}, callback=(->))->
        reqID = do mkID
        unless chat_id # TODO: fazer esse tratamento em todos os métodos (usar meta prog para não repetir tanto)
            err = Error "FAIL #{reqID} sendMessage: chat_id não foi definido"
            @logger.error err
            callback err
        logText = text.replace(/^(.{22}).*/m, '$1...').replace /\n/g, '\\n'
        @logger.log "INI #{reqID} sendMessage to #{chat_id}:", logText, params
        request.post @apiURL('sendMessage'), formData: {params..., chat_id, text}, (error, response, body)=>
            data = JSON.parse body or '{}'
            error ?= data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} sendMessage in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} sendMessage in #{chat_id}"
            callback error, response, data?.result

    sendPhoto: (file, chat_id, params={}, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} sendPhoto to #{chat_id}:", file, params
        photo = getRealFileToSend file
        request.post @apiURL('sendPhoto'), formData: {params..., chat_id, photo}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} sendPhoto in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} sendPhoto in #{chat_id}"
            callback error, response, data.result

    sendVideo: (file, chat_id, params={}, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} sendVideo in #{chat_id}:", file, params
        video = getRealFileToSend file
        request.post @apiURL('sendVideo'), formData: {params..., chat_id, video}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} sendVideo in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} sendVideo in #{chat_id}"
            callback error, response, data.result

    sendDocument: (file, chat_id, params={}, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} sendDocument to #{chat_id}:", file, params
        document = getRealFileToSend file
        document.path = params.file_name if params.file_name
        request.post @apiURL('sendDocument'), formData: {params..., chat_id, document}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} sendDocument in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} sendDocument in #{chat_id}"
            callback error, response, data?.result

    sendVoice: (file, chat_id, params={}, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} sendVoice to #{chat_id}:", file, params
        voice = getRealFileToSend file
        request.post @apiURL('sendVoice'), formData: {params..., chat_id, voice}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} sendVoice in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} sendVoice in #{chat_id}"
            callback error, response, data?.result

    sendAudio: (file, chat_id, params={}, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} sendAudio to #{chat_id}:", file, params
        audio = getRealFileToSend file
        audio.path = params.file_name if params.file_name
        if params.thumb
          params.thumb = getRealFileToSend params.thumb
        request.post @apiURL('sendAudio'), formData: {params..., chat_id, audio}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} sendAudio in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} sendAudio in #{chat_id}"
            callback error, response, data?.result

    editMessageText: (text, chat_id, message_id, params={}, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} editMessageText #{message_id} in #{chat_id}:", text, params
        request.post @apiURL('editMessageText'), formData: {params..., chat_id, message_id, text}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} editMessageText in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} editMessageText in #{chat_id}"
            callback error, response, data?.result

    editMessageTextOrReply: (text, chat_id, message_id, params={}, callback=(->))->
        @editMessageText text, chat_id, message_id, params, (err, args...)=>
            if err
                @logger.error err, 'Retry with a reply messsage...'
                @sendMessage text, chat_id, {params..., reply_to_message_id: message_id}, callback
            else
                callback err, args...

    editMessageReplyMarkup: (reply_markup, chat_id, message_id, params={}, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} editMessageReplyMarkup #{message_id} in #{chat_id}:", reply_markup, params
        request.post @apiURL('editMessageReplyMarkup'), formData: {params..., chat_id, message_id, reply_markup}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} editMessageReplyMarkup in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} editMessageReplyMarkup in #{chat_id}"
            callback error, response, data?.result

    deleteMessage: (chat_id, message_id, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} deleteMessage #{message_id} in #{chat_id}."
        request.post @apiURL('deleteMessage'), formData: {chat_id, message_id}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} deleteMessage in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} deleteMessage in #{chat_id}"
            callback error, response, data?.result

    getFile: (file_id, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} getFile #{file_id}:"
        request.post @apiURL('getFile'), formData: {file_id}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} getFile #{file_id}", error
            else
                @logger.log "SUCESS #{reqID} getFile #{file_id}"
            callback error, response, data?.result

    downlodFile: (filePath, localFile, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} downlodFile #{filePath}:"
        url="https://api.telegram.org/file/bot#{@token}/#{filePath}"
        request(url).pipe fs.createWriteStream localFile
        .on 'error', (err)=>
            @logger.error "FAIL #{reqID} downlodFile #{filePath}", error
            callback err
        .on 'finish', =>
            @logger.log "SUCESS #{reqID} downlodFile #{filePath}"
            callback null

    answerCallbackQuery: (callback_query_id, text, show_alert=true, params={}, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} answerCallbackQuery #{callback_query_id}."
        formData = form: { text, callback_query_id, show_alert, params... }
        request.post @apiURL('answerCallbackQuery'), formData, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} answerCallbackQuery #{callback_query_id}", error
            else
                @logger.log "SUCESS #{reqID} answerCallbackQuery #{callback_query_id}"
            callback error, response, data?.result

    kickChatMember: (chat_id, user_id, until_date=0, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} kickChatMember #{user_id} in #{chat_id}."
        request.post @apiURL('kickChatMember'), formData: {chat_id, user_id, until_date}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} kickChatMember in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} kickChatMember in #{chat_id}"
            callback error, response, data?.result

    unbanChatMember: (chat_id, user_id, only_if_banned=true, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} unbanChatMember #{user_id} in #{chat_id}."
        request.post @apiURL('unbanChatMember'), formData: {chat_id, user_id, only_if_banned}, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} unbanChatMember in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} unbanChatMember in #{chat_id}"
            callback error, response, data?.result

    restrictChatMember: (chat_id, user_id, permissions, until_date=0, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} restrictChatMember #{user_id} in #{chat_id}."
        formData = {chat_id, user_id, permissions, until_date}
        request.post @apiURL('restrictChatMember'), formData, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} restrictChatMember in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} restrictChatMember in #{chat_id}"
            callback error, response, data?.result

    promoteChatMember: (chat_id, user_id, params={}, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} promoteChatMember #{user_id} in #{chat_id}."
        formData = {params..., chat_id, user_id}
        request.post @apiURL('promoteChatMember'), formData, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} promoteChatMember in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} promoteChatMember in #{chat_id}"
            callback error, response, data?.result

    setChatAdministratorCustomTitle: (chat_id, user_id, custom_title, callback=(->))->
        reqID = do mkID
        @logger.log "INI #{reqID} setChatAdministratorCustomTitle #{user_id} in #{chat_id}."
        formData = {chat_id, user_id, custom_title}
        request.post @apiURL('setChatAdministratorCustomTitle'), formData, (error, response, body)=>
            data = JSON.parse body if body
            error = data if data and not data.ok
            error.message ?= error.description if error
            if error
                @logger.error "FAIL #{reqID} setChatAdministratorCustomTitle in #{chat_id}", error
            else
                @logger.log "SUCESS #{reqID} setChatAdministratorCustomTitle in #{chat_id}"
            callback error, response, data?.result

    addInteractions: (modules...)->
        @__interactions = [@__interactions..., modules...]
        @logger.log 'addInteractions:', modules.map (m)-> m.name
        mod.init this for mod in modules when mod.init?

    allowedTags = ['a','b','i','strong','em','code','pre']

    cleanHTML: (args...)-> SmallSimpleBot.cleanHTML args...
    @cleanHTML = (html, throwParseError=false)->
        # Prevent crash when malformed header:
        html = html
        .replace /^\s*<!DOCTYPE[^>]*>/s, ''
        .replace /^\s*<\?[^>]*>/s, ''
        # Normalize it:
        try
            html = minify html
        catch err
            throw err if throwParseError
            html = 'ERROR:' + err.message.substr(0, 250)
                   .replace /&/g, '&amp;'
                   .replace /</g, '&lt;'
                   .replace />/g, '&gt;'
        # Clean it:
        html = html
        .replace /\s/sg, ' '
        .replace /<\?.*?\?>/sgi, ''
        .replace /<!--.*?-->/sgi, ''
        .replace /<\/?(br|div)[^>]*>/sgi, '\n'
        .replace /<\/?p(>| [^>]*>)/sgi, '\n\n'
        .replace /<\/(ul|ol)>/sgi, '\n'
        .replace /<li[^>]*>/sgi, '\n• '
        .replace /<hr[^>]*>/sgi, '\n\n─────────────────────────\n\n'
        .replace /<h[1-6][^>]*>\s*/sgi, '\n<strong>'
        .replace /\s*<\/h[1-6][^>]*>/sgi, '</strong>\n'
        .replace /<title[^>]*>[^<]*<\/title>/sgi, ''
        .replace /<style[^>]*>[^<]*<\/style>/sgi, ''
        .replace /<script[^>]*>[^<]*<\/script>/sgi, ''
        .replace /<head[^>]*>[^<]*<\/head>/sgi, ''
        .replace /<img[^>]* alt="([^"]*)"[^>]*>/sgi, '$1'
        .replace /<\/?([a-z0-9!]+)[^>]*>/sgi, (t, name)->
            if name.toLowerCase() in allowedTags then t else ''
        .replace /<a[^>]*>\s*<\/a>/sgi, ''
        .replace /<a[^>]* (href="[^"]*")[^>]*>/sgi, '<a $1>'
        .replace /<([^a][a-z]*) [^>]*>/sgi, '<$1>'
        .replace /&#?[a-z0-9]+;/g, (e)->
            if e.match /&[lg]t;/ then e else htmlEntities.decode e
        .replace /\s*(\n\s*){2,}/sg, '\n\n'
        .replace /[ \t\r]+/sg, ' '
        .replace /[ ]*\n[ ]*/sg, '\n'
        .trim()
        @removeNestedTags html, throwParseError

    removeNestedTags: (args...)-> SmallSimpleBot.removeNestedTags args...
    @removeNestedTags = (origHtml, throwParseError)->
        html = origHtml
        .replace /\s/sg, ' '
        .replace /(<[^\/])/sg, '\r$1'
        .replace /(<\/[^>]*>)/sg, '$1\t'
        counter = 0
        while html.match /\r[^\t]+\r/
            html = html.replace /(\r[^\t]+)\r<[^>]*>([^<]*)<[^>]*>\t/sg, '$1$2'
            if counter++ > 100
                console.error '>> So many loops inside removeNestedTags!'
                console.error '>> Original HTML'
                console.error origHtml
                console.error '>> Result HTML'
                console.error html
                if throwParseError
                    throw Error 'So many loops inside removeNestedTags!'
                else
                    html = html.replace /\s/sg, ' '
        html.replace /[\r\t]/sg, ''

    limitText: (txt, opts)-> SmallSimpleBot.limitText txt, opts
    @limitText = (txt, {isHTML=false, length=3300}={})->
        minLen = if length > 40 then Math.round length/2 else 0
        if txt.length > length
            txt = txt.replace ///^(.{#{minLen},#{length-10}})((\s|\]|-|[=+*/>)}.,:;!?])|\[|<|\(|\{).*///s, '$1$3'
            txt = txt.substr 0, length-10 if txt.length > length
            if isHTML
                txt = txt.replace /<[^>]*$/si, ''
                         .replace /[-[ •*_+=(@#&]*$/si, ''
                         .trim()
                for tag in allowedTags
                    txt += "</#{tag}>" if txt.lastIndexOf('<'+tag) > txt.lastIndexOf('</'+tag)
            txt += '…'
        txt


module.exports = SmallSimpleBot

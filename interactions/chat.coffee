{ randText } = require '../helpers/random'

module.exports = simpleChat = (bot, update)->
    msg = update.message?.text or ''
    if update.toMe and msg
        if msg.match /\b(\/start|oi|ol[aá]|e a[ií]|hi)\b/i
            from = update._act.from
            txt = randText update.message.chat.id, [
                'Oi', 'Olá', 'Oi!', 'Olá!'
                'Oi {{usr}}!', 'Olá {{usr}}!'
                'Oi {{fname}}!', 'Olá {{fname}}!'
            ], {
                usr: from.username, fname: from.first_name
            }
            bot.sendMessage txt, update.message.chat.id, reply_to_message_id: update.message.message_id


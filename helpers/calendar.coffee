monthsEn = 'January February March April May June July August September October November December'.split ' '
weekEn = 'Sun Mon Tue Wed Thu Fri Sat'.split ' '

module.exports = cal = (bot)->

    selDateInlineKeyboard = (workflowId, year, month, {locale})->
        year ?= (new Date).getUTCFullYear()
        month ?= (new Date).getUTCMonth()
        prependId = "calendar-#{workflowId}-"
        firstMonthDay = new Date year, month, 1
        months = locale.month or monthsEn
        week = locale.week or weekEn
        bot.logger.log "Generate calendar InlineKeyboard to #{monthsEn[month]}/#{year}"

        prevMonth = (new Date year, month-1, 1).toISOString().substr 0,7
        nextMonth = (new Date year, month+1, 1).toISOString().substr 0,7

        keyboard = [[
            { text: '⇦', callback_data: prependId+'mv-month:' + prevMonth }
            { text: "#{months[month]}/#{year}", callback_data: 'calendar-ignore' }
            { text: '⇨', callback_data: prependId+'mv-month:' + nextMonth }
        ]]

        keyboard.push [0..6].map (i)-> text: week[i], callback_data: 'calendar-ignore'

        d = new Date firstMonthDay.getUTCFullYear(), month, -firstMonthDay.getUTCDay()+1
        while (d.getUTCFullYear()*100)+d.getUTCMonth() <= (year*100)+month
            bot.logger.log 'while', d
            line = []
            keyboard.push line
            for i in [1..7]
                if d.getUTCMonth() is month
                    line.push
                        text: d.getUTCDate()
                        callback_data: prependId+'sel-day:' + d.toISOString().split('T')[0]
                else
                    line.push text: '-', callback_data: prependId+'calendar-ignore'
                d.setUTCDate d.getUTCDate() + 1

        JSON.stringify inline_keyboard: keyboard


    parseDateInlineKeyboard = (workflowId, update, {locale}, callback)->
        prependId = "calendar-#{workflowId}-"
        if update.callback_query? and update.callback_query?.data?.indexOf(prependId) is 0
            cmd = update.callback_query.data.substr(prependId.length).split(':')
            msgId = update.callback_query.message?.message_id
            chat = update.callback_query.message?.chat.id
            year = null
            month = null
            if cmd[0] is 'mv-month'
                year = parseInt cmd[1].split('-')[0]
                month = parseInt cmd[1].split('-')[1]-1 # we need zero indexed month num.
                keyboard = selDateInlineKeyboard workflowId, year, month, {locale}
                bot.editMessageReplyMarkup keyboard, chat, msgId
            if cmd[0] is 'sel-day'
                bot.editMessageReplyMarkup '{"inline_keyboard":[]}', chat, msgId
                callback bot, (new Date cmd[1]), update


    { selDateInlineKeyboard, parseDateInlineKeyboard }



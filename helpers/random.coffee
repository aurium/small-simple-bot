{ timeout, interval } = require './timing'

lastGlobalRandChoicesMemo = {}
clearMemoTimeouts = {}
clearMemoDelay = 60*60*2

if process.env.NODE_ENV
    exports.lastGlobalRandChoicesMemo = lastGlobalRandChoicesMemo
    exports.setClearMemoDelay = (val)-> clearMemoDelay = val

filterUsedOptions = (options, lastRandChoices)->
    options.filter (opt)-> lastRandChoices.indexOf(opt) is -1

exports.randText = (cacheKey, options, replacer={})->
    lastGlobalRandChoicesMemo[cacheKey] ?= []
    lastRandChoices = lastGlobalRandChoicesMemo[cacheKey]
    clearTimeout clearMemoTimeouts[cacheKey]
    clearMemoTimeouts[cacheKey] = timeout clearMemoDelay, ->
        delete lastGlobalRandChoicesMemo[cacheKey]
    nonusedOptions = filterUsedOptions options, lastRandChoices
    if nonusedOptions.length is 0
        if options.length > 1
            do lastRandChoices.shift while lastRandChoices.length > 1
            nonusedOptions = filterUsedOptions options, lastRandChoices
        else
            nonusedOptions = [...options]
    choice = nonusedOptions[ Math.floor nonusedOptions.length * Math.random() ]
    lastRandChoices.push choice
    for key, value of replacer
        choice = choice.replace ///\{\{#{key}\}\}///g, value
    choice


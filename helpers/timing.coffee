exports.timeout = (secs, callback)-> setTimeout callback, secs*1000
exports.interval = (secs, callback)-> setInterval callback, secs*1000


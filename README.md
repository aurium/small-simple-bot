Small Simple Bot
================

A Really simple Telegram Bot

Usage
-----

### In your desktop / Local network

Write `my-bot.coffee`:
```coffee
SmallSimpleBot = require 'small-simple-bot'

bot = new SmallSimpleBot
    token: '123456789:YourBotsTelegramToken'
    adms: [
        { username: 'aurium', id: 123456789 }
        { username: 'diguliu', id: 987654321 }
    ]

bot.addInteractions(
    require './my-interaction-module'
    require './other-interaction-module'
)

do bot.run
```

### In a server

Write `my-bot.coffee`:
```coffee
SmallSimpleBot = require 'small-simple-bot'

bot = new SmallSimpleBot
    token: '123456789:YourBotsTelegramToken'
    webhookPath: 'hidden-end-point'
    adms: [
        { username: 'aurium', id: 123456789 }
        { username: 'diguliu', id: 987654321 }
    ]

bot.addInteractions(
    require './my-interaction-module'
    require './other-interaction-module'
)

server = do require('http').createServer

bot.bindServer server

server.listen process.env.PORT or 3000
```


Create an interation module
---------------------------

Write `my-interaction-module.coffee`:
```coffee
module.exports = (bot, update)->
    msg = update.message?.text or ''
    if update.toMe and msg
        bot.sendMessage "You said #{msg}", update.message.chat.id
```


